import * as React from 'react';
import './App.css';

// custom component
import Game from './component/Game';

class App extends React.Component {
  public render() {
    return <Game />;
  }
}

export default App;
