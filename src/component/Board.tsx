import * as React from 'react';
import { css } from 'aphrodite';

// custom component
import Square from './Square';

// custom style
import BoardStyle from '../style/BoardStyle';

interface IProps {
  squares: string[];
  onClick: any;
}

function renderSquare(square: string, onClick: any) {
  return <Square value={square} onClick={onClick} />;
}

function board(props: IProps) {
  const { squares, onClick } = props;
  return (
    // initiate the board game
    <div>
      <div className={css(BoardStyle.rowBoard)}>
        {renderSquare(squares[0], () => onClick(0))}
        {renderSquare(squares[1], () => onClick(1))}
        {renderSquare(squares[2], () => onClick(2))}
      </div>
      <div className={css(BoardStyle.rowBoard)}>
        {renderSquare(squares[3], () => onClick(3))}
        {renderSquare(squares[4], () => onClick(4))}
        {renderSquare(squares[5], () => onClick(5))}
      </div>
      <div className={css(BoardStyle.rowBoard)}>
        {renderSquare(squares[6], () => onClick(6))}
        {renderSquare(squares[7], () => onClick(7))}
        {renderSquare(squares[8], () => onClick(8))}
      </div>
    </div>
  );
}

export default board;
