import { css } from 'aphrodite';
import * as React from 'react';
// custom func
import TicTacToe from '../algorithm/TicTacToe';
// custom style
import BoardStyle from '../style/BoardStyle';
// custom component
import Board from './Board';

const Component = React.Component;

interface IStates {
  history: { squares: string[] }[];
  stepNumber: number;
  xIsNext: boolean;
  isEmpty: boolean;
}

class Game extends Component<{}, IStates> {
  constructor(props: any) {
    super(props);
    this.state = {
      history: [{ squares: Array(9).fill(null) }], // histories of step
      stepNumber: 0, // step of choice
      xIsNext: true, // determine the step => x is You
      isEmpty: false, // determine if the step is already the end
    };

    this.handleClick = this.handleClick.bind(this);
  }

  // execution of random choice from computer
  componentWillUpdate(nextProps: any, nextState: IStates) {
    const { history, xIsNext, stepNumber } = nextState;

    if (!xIsNext) {
      const current = history[stepNumber];
      const squares = current.squares.slice();
      let empties = 0;
      let index = 0;
      const availableIndex: number[] = [];
      for (const square of current.squares) {
        if (!square) {
          empties = empties + 1;
          availableIndex.push(index);
        }
        index = index + 1;
      }
      let randomAi = null;
      while (randomAi == null || (randomAi && randomAi < 0 && empties > 0)) {
        randomAi = Math.floor(Math.random() * empties - 1);
      }
      if (randomAi !== null) {
        squares[availableIndex[randomAi]] = 'O';

        this.setState({
          history: history.concat([{ squares }]),
          stepNumber: history.length,
          xIsNext: true,
        });
      }

      this.setState({
        isEmpty: empties === 0,
      });
    }
  }

  handleClick(i: number) {
    const { stepNumber, xIsNext } = this.state;
    let { history } = this.state;
    history = history.slice(0, stepNumber + 1);

    const current = history[history.length - 1];
    const squares = current.squares.slice();

    if (TicTacToe.calculateWinner(squares) || squares[i]) {
      return;
    }

    squares[i] = xIsNext ? 'X' : 'O';

    this.setState({
      history: history.concat([{ squares }]),
      stepNumber: history.length,
      xIsNext: !xIsNext,
    });
  }

  jumpTo(stepNumber: number) {
    if (stepNumber === 0) {
      this.setState({
        isEmpty: false,
        history: [{ squares: Array(9).fill(null) }],
      });
    }

    this.setState({
      stepNumber,
      xIsNext: stepNumber % 2 === 0,
    });
  }

  render() {
    const { history, stepNumber, isEmpty } = this.state;
    const current = history[stepNumber];
    const winner = TicTacToe.calculateWinner(current.squares);

    const moves = history.map((step, move) => {
      const desc = move ? `Go to move #${move}` : 'Go to game start';
      return move % 2 !== 0 || move === 0 ? (
        <li key={move}>
          <button onClick={() => this.jumpTo(move)}>{desc}</button>
        </li>
      ) : null;
    });

    let status;
    if (winner) {
      status = `Winner: ${winner === 'X' ? 'You' : 'Computer'}`;
    } else if (!winner && isEmpty) {
      status = 'Tie';
    } else {
      status = 'Your turn';
    }

    return (
      <div>
        <div className={css(BoardStyle.game)}>
          <div>
            <Board
              squares={current.squares}
              onClick={(i: number) => this.handleClick(i)}
            />
          </div>
          <div className={css(BoardStyle.gameInfo)}>
            <div>{status}</div>
            <ol>{moves}</ol>
          </div>
        </div>
      </div>
    );
  }
}

export default Game;
