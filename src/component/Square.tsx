import * as React from 'react';
import { css } from 'aphrodite';
import { IoIosContact, IoIosOutlet } from 'react-icons/io';

// custom style
import BoardStyle from '../style/BoardStyle';

interface IProps {
  onClick: any;
  value: string;
}

function square(props: IProps) {
  const { onClick, value } = props;
  // give symbol to each players
  let out = null;
  switch (value) {
    case 'X':
      out = <IoIosContact />;
      break;
    case 'O':
      out = <IoIosOutlet />;
      break;
    default:
      break;
  }
  return (
    <button className={css(BoardStyle.square)} onClick={onClick}>
      {out}
    </button>
  );
}

export default square;
