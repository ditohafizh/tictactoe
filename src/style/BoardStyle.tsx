import { StyleSheet } from 'aphrodite';
import Colors from './Colors';

export default StyleSheet.create({
  column: {
    paddingLeft: '30px',
  },
  rowBoard: {
    ':after': {
      clear: 'both',
      content: '',
      display: 'table',
    },
  },
  square: {
    background: Colors.pureWhite,
    border: `1px solid ${Colors.mediumBlack}`,
    float: 'left',
    fontSize: '24px',
    fontWeight: 'bold',
    lineHeight: '3rem',
    height: '3rem',
    marginRight: '-1px',
    marginTop: '-1px',
    padding: 0,
    textAlign: 'center',
    width: '3rem',

    ':focus': {
      outline: 'none',
    },
  },
  game: {
    display: 'flex',
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    paddingTop: '5rem',
    paddingBottom: '5rem',
  },
  gameInfo: {
    marginLeft: '20px',
  },
});
